# sample

This is the general structure of the project:

outfittery/
├── build.sbt
├── derby.log

├── Posts.xml
├── project
│   ├── build.properties
│   └── target
│       ├── config-classes
│       │   ├── $0f9c0d6962611ef05e28.cache
...
│       ├── scala-2.12
│       │   └── sbt-1.0
│       │       └── resolution-cache


├── spark-warehouse
│   └── outfittery_hive
│       ├── part-00000-7e41ffce-090a-48e1-a2fe-e03d28d0aa08-c000.snappy.parquet

// all parquet files here

│       └── _SUCCESS
├── src
│   └── main
│       └── scala
│           ├── stack.scala

├── target
│   ├── scala-2.11
│   │   ├── classes
│   │   │   ├── stack.class
│   │   │   └── stack$.class
│   │   ├── resolution-cache
│   │   │   ├── ch.epfl.scala
│   │   │   │   └── stack_2.11
│   │   │   │       └── 1.0
│   │   │   │           ├── resolved.xml.properties
│   │   │   │           └── resolved.xml.xml
│   │   │   └── reports
│   │   │       ├── ch.epfl.scala-stack_2.11-compile-internal.xml
...
│   │   │       ├── ivy-report.css
│   │   │       └── ivy-report.xsl
│   │   └── stack_2.11-1.0.jar
│   └── streams
│       ├── compile

├── Users.xml


How to run ?
In unix/linux shell run below command.
./run.sh

/* It contains the command to compile, package and run the spark-submit command */

The solution is for only first case in the assignment -
- Total number of posts created

Design choices:
I chose to do it in Spark (even though i ran it locally the optimal decision should be
how to run it in production). Spark has cluster approach and we can use it's streaming feature.
Even though i did not use any streaming structure but as suggested in the document i
could have used one the the structures as streaming.
For testing/dev purpose i did testing using shell script but for production i would
have used this program as a DAG file in airflow and implemented the solution on most preferably 
GCP(google cloud platform) . This solution could also be run in hadoop sqoop.

Issues faced:
- The data (Users.xml and Posts.xml files having attributes names starting with caps)
- While joining data structures normal issues of joining (duplicates - remove with drop duplicates, nulls - use inner join
 instead of any other join)
