sbt compile
echo ?$
if [ $? -eq 0 ]
then
  sbt package
fi

if [ $? -eq 0 ]
then
  $SPARK_HOME/bin/spark-submit --master local[*] --packages com.databricks:spark-xml_2.11:0.4.1 target/scala-2.11/stack_2.11-1.0.jar
fi