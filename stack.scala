import org.apache.spark.sql.SparkSession
import com.databricks.spark.xml._
import org.apache.spark.sql.types.{StructType, StructField, DoubleType,StringType}
import org.apache.spark.sql.{Row, SaveMode}
import org.apache.spark.sql._
import org.apache.spark.sql.functions._


object stack {
  def main(args: Array[String]){
    val spark = SparkSession.builder.enableHiveSupport().getOrCreate()

    val df_user = spark.read
    .option("rowTag", "row")
    .format("xml")
    .load("Users.xml")
    
    df_user.printSchema()
    
    val df_post = spark.read
    .option("rowTag", "row")
    .format("xml")
    .load("Posts.xml")
    
    df_post.printSchema()
    
    import spark.sql
    import spark.implicits._
    
    /* 
      Join User and Post dataset based on common key and aggregate on count
      grouping by key in Post. 
    */
    val dataDIR = "dummy"

    val user_post_df = df_user.join(df_post.select("_owneruserid","_acceptedanswerid"), df_user("_accountId") === df_post("_owneruserid")) 
    
    val user_post_df_agg = user_post_df.groupBy("_owneruserid").agg(count("*").alias("total_no_posts"))
    
    val user_post_df_agg_joined = user_post_df.join(user_post_df_agg, "_owneruserid").dropDuplicates() 
    
    /* 
      Create external hive table as parquet dataset 
    */
    sql("CREATE EXTERNAL TABLE IF NOT EXISTS outfittery_hive(_id INT, _reputation INT, _creationDate DATE, _displayName STRING, _lastAccessDate DATE, _websiteUrl STRING, _location STRING, _aboutMe STRING, _views INT, _upVotes INT, _downVotes INT, _accountId INT, _owneruserid INT, _answercount INT) STORED AS PARQUET LOCATION '$dataDIR'")

    user_post_df_agg_joined.write.mode("overwrite").saveAsTable("outfittery_hive")
    
    sql("SELECT * FROM outfittery_hive limit 5").show() 
    spark.stop()
 }
}